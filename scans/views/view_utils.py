import ipywidgets


class ViewUtils:
    @staticmethod
    def build_label(text):
        return ipywidgets.HTML(f"<strong>{text}</strong>")

    @staticmethod
    def sort_options(options):
        return sorted(options, key=lambda option: option[0])

    @staticmethod
    def devices_to_options(devices):
        options = [(dev.name, dev) for dev in devices]
        return sorted(options, key=lambda opt: opt[0].split(".")[-1])

    @staticmethod
    def format_number(value, units_name="", precision=3):
        if value is None:
            return f"? {units_name}"
        return f"{round(value, precision)} {units_name}"
