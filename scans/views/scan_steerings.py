import ipywidgets

from ..models import Steering
from .view import View
from .view_utils import ViewUtils


class ScanSteeringsView(View):
    LABEL_TEXT = "Steering selection"

    def __init__(self, observers=None, available_steerings=[]):
        super().__init__(observers)
        self._available_steerings = []
        self._init_widget()
        self.available_steerings = available_steerings

    def _init_widget(self):
        self._steering_widget = ipywidgets.Dropdown(
            options=ViewUtils.devices_to_options(self._available_steerings),
            value=None,
            rows=10,
        )
        self._steering_widget.observe(self._on_steering_selection)

        self._widget = ipywidgets.VBox(
            [
                ViewUtils.build_label(ScanSteeringsView.LABEL_TEXT),
                self._steering_widget,
            ]
        )

    def _on_steering_selection(self, change):
        if change["name"] != "value":
            return

        self._notify_observers(
            ("steering selection", {"new": change["new"], "old": change["old"]})
        )

    def refresh(self):
        pass

    @property
    def available_steerings(self):
        return self._available_steerings

    @property
    def selected_steering(self):
        if self._steering_widget.value:
            return self._steering_widget.value
        else:
            return None

    @available_steerings.setter
    def available_steerings(self, steerings):
        assert all(isinstance(steering, Steering) for steering in steerings)
        self._available_steerings = steerings
        self._steering_widget.value = None
        options = ViewUtils.devices_to_options(self._available_steerings)
        self._steering_widget.options = options
