import ipywidgets

from ..models import Collimator, CollimatorSettings
from .view import View
from .view_utils import ViewUtils


class ScanSettingsCollimatorView(View):
    def __init__(self, collimator, observers=None):
        super().__init__(observers)
        self._settings = CollimatorSettings()
        self._widget = ipywidgets.HBox([])
        self.collimator = collimator
        self._init_widget()

    def _init_widget(self):
        self._center_start_input = ipywidgets.BoundedFloatText(
            value=self.settings.start,
            min=self.steering.min,
            max=self.steering.max,
            step=None,
            description="Center start:",
        )

        self._center_end_input = ipywidgets.BoundedFloatText(
            value=self.settings.end,
            min=self.steering.min,
            max=self.steering.max,
            step=None,
            description="Center end:",
        )

        self._steps_input = ipywidgets.BoundedIntText(
            value=self.settings.step,
            min=self.settings.min_step,
            max=self.settings.max_step,
            description="Steps:",
        )

        self._slit_input = ipywidgets.BoundedFloatText(
            value=self.settings.slit,
            min=self.steering.min_slit,
            max=self.steering.max_slit,
            step=None,
            description="Slit size:",
        )

        self._tolerance_input = ipywidgets.BoundedFloatText(
            value=self.settings.tolerance,
            min=0,
            max=self.steering.max_slit / 2,
            step=None,
            description="Tolerance:",
        )

        self._center_start_input.observe(self._update_model, names="value")
        self._center_end_input.observe(self._update_model, names="value")
        self._steps_input.observe(self._update_model, names="value")
        self._slit_input.observe(self._update_model, names="value")
        self._tolerance_input.observe(self._update_model, names="value")

        units = self.steering.units_name
        jaw1 = self.steering.jaw1_name
        jaw2 = self.steering.jaw2_name

        self._widget.children = [
            ipywidgets.VBox(
                [
                    ipywidgets.HBox(
                        [
                            self._center_start_input,
                            ipywidgets.Label(
                                f"Min: {ViewUtils.format_number(self.steering.min, units)}"
                            ),
                        ]
                    ),
                    ipywidgets.HBox(
                        [
                            self._center_end_input,
                            ipywidgets.Label(
                                f"Max: {ViewUtils.format_number(self.steering.max, units)}"
                            ),
                        ]
                    ),
                    self._steps_input,
                    self._slit_input,
                    self._tolerance_input,
                ],
                layout=ipywidgets.Layout(width="60%"),
            ),
            ipywidgets.VBox(
                [
                    ipywidgets.Label(f"Orientation: {self.steering.orientation.name}"),
                    ipywidgets.Label(
                        f"{jaw1} Ref.: {ViewUtils.format_number(self.steering.pos1_ref, units)}"
                    ),
                    ipywidgets.Label(
                        f"{jaw2} Ref.: {ViewUtils.format_number(self.steering.pos2_ref, units)}"
                    ),
                    ipywidgets.Label(
                        f"{jaw1}: {ViewUtils.format_number(self.steering.pos1, units)}"
                    ),
                    ipywidgets.Label(
                        f"{jaw2}: {ViewUtils.format_number(self.steering.pos2, units)}"
                    ),
                    ipywidgets.Label(
                        f"Center: {ViewUtils.format_number(self.steering.center, units)}"
                    ),
                    ipywidgets.Label(
                        f"Slit: {ViewUtils.format_number(self.steering.slit, units)}"
                    ),
                ]
            ),
        ]

    def _update_model(self, action=None):
        self._settings.start = self._center_start_input.value
        self._settings.end = self._center_end_input.value
        self._settings.step = self._steps_input.value
        self._settings.slit = self._slit_input.value
        self._settings.tolerance = self._tolerance_input.value

    def refresh(self):
        self._init_widget()
        self._update_model()

    @property
    def settings(self):
        return self._settings

    @property
    def steering(self):
        return self._collimator

    @property
    def collimator(self):
        return self._collimator

    @collimator.setter
    def collimator(self, value):
        assert isinstance(value, Collimator)

        self._collimator = value
        self.refresh()
