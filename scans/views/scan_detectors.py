import ipywidgets

from .view import View
from .view_utils import ViewUtils
from ..models import Detector


class ScanDetectorsView(View):
    LABEL_TEXT = "Detectors selection"

    def __init__(self, observers=None, available_detectors=None):
        super().__init__(observers)
        self._available_detectors = []
        self._init_widget()
        self.available_detectors = available_detectors

    def _init_widget(self):
        self._widget = ipywidgets.VBox([])

        self._left_widget = self._build_left_widget()
        self._right_widget = self._build_right_widget()
        self._right_button = self._build_right_button()
        self._right_button_all = self._build_right_button_all()
        self._left_button = self._build_left_button()
        self._left_button_all = self._build_left_button_all()

        self._widget.children = (
            ViewUtils.build_label(ScanDetectorsView.LABEL_TEXT),
            ipywidgets.HBox(
                [
                    self._left_widget,
                    ipywidgets.VBox(
                        [
                            self._right_button,
                            self._right_button_all,
                            self._left_button,
                            self._left_button_all,
                        ]
                    ),
                    self._right_widget,
                ]
            ),
        )

    def _build_left_widget(self):
        return ipywidgets.SelectMultiple(
            options=ViewUtils.devices_to_options(self.available_detectors),
            value=(),
            rows=10,
        )

    def _build_right_widget(self):
        return ipywidgets.SelectMultiple(options=[], value=(), rows=10)

    def _build_right_button(self):
        button = ipywidgets.Button(description=">")
        button.on_click(self._on_right_button)
        return button

    def _build_right_button_all(self):
        button = ipywidgets.Button(description=">>")
        button.on_click(self._on_right_button_all)
        return button

    def _build_left_button(self):
        button = ipywidgets.Button(description="<")
        button.on_click(self._on_left_button)
        return button

    def _build_left_button_all(self):
        button = ipywidgets.Button(description="<<")
        button.on_click(self._on_left_button_all)
        return button

    def _on_right_button(self, _):
        selected = ViewUtils.devices_to_options(self._left_widget.value)
        left = [opt for opt in self._left_widget.options if opt not in selected]

        self._left_widget.options = left
        self._right_widget.options = ViewUtils.sort_options(
            (*self._right_widget.options, *selected)
        )

    def _on_right_button_all(self, _):
        selected = self._left_widget.options
        self._left_widget.options = ()
        self._right_widget.options = ViewUtils.sort_options(
            (*self._right_widget.options, *selected)
        )

    def _on_left_button(self, _):
        selected = ViewUtils.devices_to_options(self._right_widget.value)
        right = [opt for opt in self._right_widget.options if opt not in selected]

        self._right_widget.options = right
        self._left_widget.options = ViewUtils.sort_options(
            (*self._left_widget.options, *selected)
        )

    def _on_left_button_all(self, _):
        selected = self._right_widget.options
        self._right_widget.options = ()
        self._left_widget.options = ViewUtils.sort_options(
            (*self._left_widget.options, *selected)
        )

    def refresh(self, action=None):
        pass

    @property
    def available_detectors(self):
        return self._available_detectors

    @property
    def selected_detectors(self):
        return [option[1] for option in self._right_widget.options]

    @available_detectors.setter
    def available_detectors(self, detectors):
        assert all(isinstance(detector, Detector) for detector in detectors)
        self._available_detectors = detectors
        self._on_left_button_all(
            None
        )  # move everything from the right widget to the left one
        self._left_widget.options = ViewUtils.devices_to_options(
            self._available_detectors
        )
