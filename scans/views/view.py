from abc import ABC, abstractmethod
from IPython.display import display


class View(ABC):
    def __init__(self, observers=None):
        self._widget = None
        self._set_observers(observers)

    def _ipython_display_(self):
        if self._widget is None:
            raise RuntimeError("View is not initialized")
        display(self._widget)

    def _set_observers(self, observers):
        self._observers = []
        if observers is not None:
            for observer in observers:
                self.add_observer(observer)

    def _notify_observers(self, *args, **kwargs):
        for observer in self._observers:
            observer.update(*args, **kwargs)

    @abstractmethod
    def refresh(self, action):
        pass

    def add_observer(self, observer):
        self._observers.append(observer)

    @property
    def observers(self):
        return self._observers

    @property
    def widget(self):
        return self._widget
