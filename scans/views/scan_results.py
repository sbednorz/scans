import ipywidgets
import datetime
import pandas as pd
import numpy as np
from plotly import graph_objs as go
from IPython.display import display

from .view import View
from .view_utils import ViewUtils
from ..models import Collimator, Magnet


class ScanResultsView(View):
    BLANK_DF_VALUE = np.nan

    def __init__(self, detectors, steering, settings, normalization, observers=None):
        super().__init__(observers)
        self._detectors = detectors
        self._steering = steering
        self._settings = settings
        self._normalization = normalization

        self._current_step = 0
        self._df = self._build_df()

        self._figs = self._build_figs()
        self._figs_outputs = self._build_figs_outputs(self._figs)
        self._figs_widget = self._build_figs_widget(self._figs_outputs)

        self._state = None
        self._state_widget = self._build_state_widget()

        self._init_widget()

    def _init_widget(self):
        self._output = ipywidgets.Output()

        self._widget = ipywidgets.VBox(
            [
                ViewUtils.build_label("Scan results"),
                self._state_widget,
                self._output,
                self._figs_widget,
            ]
        )
        self.refresh()

    def _build_figs(self):
        figs = []
        tickvals = self.settings.get_step_poses()
        for detector in self.detectors:
            fig = go.FigureWidget()
            fig.add_trace(
                go.Bar(x=[], y=[], name=detector.name, marker=dict(color="black"))
            )
            fig.update_layout(
                showlegend=False,
                margin=dict(l=0, r=0, t=50, b=0),
                width=250,
                height=300,
                title=detector.name,
            )
            fig.update_xaxes(tickvals=tickvals, ticktext=tickvals)
            figs.append(fig)

        return figs

    def _build_figs_outputs(self, figs):
        outputs = []
        for fig in figs:
            output = ipywidgets.Output()
            with output:
                display(fig)
            outputs.append(output)
        return outputs

    def _build_figs_widget(self, outputs):
        return ipywidgets.HBox(outputs)

    def _build_df(self):
        cols = {
            "Step": list(range(self.settings.step)),
            f"{self.steering.name} (Set)": self._build_df_steering_set_row(),
            f"{self.steering.name} (Read)": self._build_df_empty_row(),
            "Normalization": self._build_df_empty_row(),
            **self._build_df_detector_rows(),
            "Timestamp": self._build_df_empty_row(),
        }
        return pd.DataFrame(cols).set_index("Step")

    def _build_state_widget(self):
        return ipywidgets.HTML("")

    def _build_df_steering_set_row(self):
        return self.settings.get_step_poses()

    def _build_df_empty_row(self):
        return [ScanResultsView.BLANK_DF_VALUE for _ in range(self.settings.step)]

    def _build_df_detector_rows(self):
        return {det.name: self._build_df_empty_row() for det in self.detectors}

    def _get_steering_read(self):
        if isinstance(self.steering, Collimator):
            return self.steering.center
        elif isinstance(self.steering, Magnet):
            return self.steering.current
        else:
            raise ValueError("Unknown steering type")

    def update(self, action):
        type, args = action

        if type == "state":
            self._state = args
            self._refresh_state()
        elif type == "result":
            detectors_read = args[0]
            normalization_read = args[1]
            step_no = self._current_step
            steering_read = self._get_steering_read()
            timestamp = datetime.datetime.now()  # TODO: better timing source

            self.append_step_result(
                step_no, steering_read, normalization_read, detectors_read, timestamp
            )

            self._current_step += 1
            self.refresh()

    def _refresh_state(self):
        state = "NOT STARTED" if self._state is None else self._state.name
        self._state_widget.value = "State: " + state

    def _refresh_df(self):
        self._output.outputs = ()
        self._output.append_display_data(self._df)

    def _refresh_figs(self):
        pass  # Figures refresh on data change

    def refresh(self, action=None):
        self._refresh_df()
        self._refresh_figs()
        self._refresh_state()

    def append_step_result(
        self, step_no, steering_read, normalization_read, detectors_read, timestamp
    ):
        assert 0 <= step_no <= self.settings.step
        assert isinstance(self._df, pd.DataFrame)
        # TODO: assert normalization

        # TODO: move column strings to class member or sth
        self._df.at[step_no, f"{self.steering.name} (Read)"] = np.float64(steering_read)
        self._df.at[step_no, "Normalization"] = (
            np.float64(normalization_read) or ScanResultsView.BLANK_DF_VALUE
        )
        self._df.at[step_no, "Timestamp"] = timestamp

        self._append_detectors_read(step_no, detectors_read)

    def _append_detectors_read(self, step_no, detectors_read):
        assert isinstance(detectors_read, dict)
        assert len(detectors_read) == len(self.detectors)

        # TODO: this updates the plot in real time so we need to meke it optimal
        for detector_idx, (detector, result) in enumerate(detectors_read.items()):
            step = self.settings.get_step_poses()[step_no]

            self._df.at[step_no, detector.name] = result

            self._figs[detector_idx].data[0]["x"] = [
                *self._figs[detector_idx].data[0]["x"],
                step,
            ]

            self._figs[detector_idx].data[0]["y"] = [
                *self._figs[detector_idx].data[0]["y"],
                result,
            ]

    @property
    def df(self):
        return self._df

    @property
    def detectors(self):
        return self._detectors

    @property
    def steering(self):
        return self._steering

    @property
    def settings(self):
        return self._settings
