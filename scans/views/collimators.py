import ipywidgets
import time
import datetime
import threading
import pandas as pd

from .view import View
from ..models import Collimator, AcquisitionStatus
from ..cesar import COLLIMATOR_NAMES


class CollimatorsView(View):
    # TODO: refactor to DeviceView and inherit from it

    def __init__(self, collimators=None, observers=None):
        super().__init__(observers)
        self._steerings = (
            collimators
            if collimators is not None
            else [Collimator(name) for name in COLLIMATOR_NAMES]
        )
        self._df = self._build_df()
        self._running = False
        self._thread = threading.Thread(target=self._thread_loop)

        self._init_widget()
        self.start()

    def _init_widget(self):
        self._output = ipywidgets.Output()
        self._output.append_display_data(self._df)

        self._time_widget = self._build_time_widget()

        self._widget = ipywidgets.VBox()
        self.refresh()

    def _get_time_str(self):
        ts = time.time()
        date = datetime.datetime.fromtimestamp(ts)
        return str(date)

    def _build_time_widget(self):
        time = self._get_time_str()
        str = f"Last update: {time}"
        return ipywidgets.Label(str)

    def _build_df(self):
        dict = {
            "Name": [steering.name for steering in self.steerings],
            "Jaw 1": [steering.pos1 for steering in self.steerings],
            "Jaw 2": [steering.pos2 for steering in self.steerings],
            "Ref. Jaw 1": [steering.pos1_ref for steering in self.steerings],
            "Ref. Jaw 2": [steering.pos2_ref for steering in self.steerings],
            "Min": [steering.min for steering in self.steerings],
            "Max": [steering.max for steering in self.steerings],
            "Motor 1": [steering.motor1.pos_status.name for steering in self.steerings],
            "Motor 2": [steering.motor2.pos_status.name for steering in self.steerings],
        }
        df = pd.DataFrame(dict).set_index("Name")
        df = df.style.apply(CollimatorsView._highlight_df, axis=1)
        return df

    @staticmethod
    def _highlight_df(x):
        if (
            x["Motor 1"] == AcquisitionStatus.BUSY.name
            or x["Motor 2"] == AcquisitionStatus.BUSY.name
        ):
            color = "yellow"
        else:
            color = ""

        return [f"background-color: {color}" for _ in x]

    def _thread_loop(self):
        while self._running:
            self.refresh()
            time.sleep(1)

    def start(self):
        self._running = True
        self._thread.start()

    def stop(self):
        self._running = False
        self._thread.join()

    def refresh(self, action=None):
        self._time_widget = self._build_time_widget()

        for steering in self.steerings:
            steering.acquire()

        self._df = self._build_df()
        self._output = ipywidgets.Output()
        self._output.append_display_data(self._df)

        self._widget.children = [
            self._time_widget,
            self._output,
        ]

    @property
    def steerings(self):
        return self._steerings
