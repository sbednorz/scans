import ipywidgets

from ..models import Normalization
from .view import View
from .view_utils import ViewUtils


class ScanNormalizationsView(View):
    LABEL_TEXT = "Normalization selection"

    def __init__(self, observers=None, normalizations=None):
        self._norms = None
        self._widget = None
        self._norm_widget = None

        super().__init__(observers)
        self._init_norms(normalizations)
        self._init_widget()

    def _init_norms(self, norms):
        if norms is None:
            self._norms = []
        else:
            assert all(isinstance(norm, Normalization) for norm in norms)
            self._norms = norms

    def _init_widget(self):
        self._norm_widget = ipywidgets.Dropdown(
            options=ScanNormalizationsView.normalizations_to_options(self._norms),
            value=None,
        )
        self._norm_widget.observe(self._on_selection, names="value")

        self._widget = ipywidgets.VBox(
            [
                ViewUtils.build_label(ScanNormalizationsView.LABEL_TEXT),
                self._norm_widget,
            ]
        )

    @staticmethod
    def normalizations_to_options(normalizations):
        options = [(norm.name, norm) for norm in normalizations]
        return sorted(options, key=lambda opt: opt[0].split(".")[-1])

    def _on_selection(self, change):
        if change["name"] != "value":
            return

        self._notify_observers(
            ("normalization selection", {"new": change["new"], "old": change["old"]})
        )

    def refresh(self):
        pass

    @property
    def available_normalizations(self):
        return self._norms

    @property
    def selected_normalization(self):
        assert self._norm_widget is not None
        return self._norm_widget.value

    @available_normalizations.setter
    def available_normalizations(self, normalizations):
        assert all(isinstance(norm, Normalization) for norm in normalizations)
        assert self._norm_widget is not None

        self._norms = normalizations
        self._norm_widget.value = None
        self._norm_widget.options = ScanNormalizationsView.normalizations_to_options(
            self._norms
        )
