import ipywidgets

from ..models import Collimator, Magnet
from ..handlers import ScanCollimatorHandler, ScanMagnetHandler
from ..cesar import (
    DEFAULT_AVAILABLE_DETECTORS,
    DEFAULT_AVAILABLE_STEERINGS,
    DEFAULT_AVAILABLE_NORMALIZATIONS,
)
from .view import View
from .scan_detectors import ScanDetectorsView
from .scan_steerings import ScanSteeringsView
from .scan_settings import ScanSettingsView
from .scan_results import ScanResultsView
from .scan_normalizations import ScanNormalizationsView


class ScanView(View):
    def __init__(
        self,
        observers=None,
        available_detectors=None,
        available_steerings=None,
        available_normalizations=None,
    ):
        if available_detectors is None:
            available_detectors = DEFAULT_AVAILABLE_DETECTORS
        if available_steerings is None:
            available_steerings = DEFAULT_AVAILABLE_STEERINGS
        if available_normalizations is None:
            available_normalizations = DEFAULT_AVAILABLE_NORMALIZATIONS

        self._detectors_view = None
        self._steerings_view = None
        self._normalizations_view = None
        self._settings_view = None
        self._start_button = None
        self._stop_button = None
        self._results_view = None

        self._handler = None

        super().__init__(observers)
        self._init_widget(
            available_detectors, available_steerings, available_normalizations
        )

    def _init_widget(
        self, available_detectors, available_steerings, available_normalizations
    ):
        self._detectors_view = ScanDetectorsView([], available_detectors)

        self._steerings_view = ScanSteeringsView([self], available_steerings)

        self._settings_view = ScanSettingsView([])

        self._normalizations_view = ScanNormalizationsView([], available_normalizations)

        self._start_button = self._build_start_button()

        self._stop_button = self._build_stop_button()

        self._widget = ipywidgets.VBox(
            [
                self._detectors_view.widget,
                self._steerings_view.widget,
                self._normalizations_view.widget,
                self._settings_view.widget,
                ipywidgets.HBox([self._start_button, self._stop_button]),
            ]
        )

        self._add_observers_to_children()

    def _build_start_button(self):
        button = ipywidgets.Button(
            description="Start scan",
            icon="play-circle",
            button_style="success",
            disabled=False,
        )
        button.on_click(self._on_start_button)
        return button

    def _build_stop_button(self):
        button = ipywidgets.Button(
            description="Stop scan",
            icon="times-circle",
            button_style="danger",
            disabled=True,
        )
        button.on_click(self._on_stop_button)
        return button

    def _on_start_button(self, event):
        assert self._settings_view is not None
        assert self._settings_view.settings is not None
        assert self._detectors_view is not None
        assert self._normalizations_view is not None
        assert self._start_button is not None
        assert self._stop_button is not None

        self._start_button.disabled = True
        self._stop_button.disabled = False

        settings = self._settings_view.settings
        steering = self._settings_view.steering
        detectors = self._detectors_view.selected_detectors
        normalization = self._normalizations_view.selected_normalization
        self._notify_observers(
            (
                "start",
                {
                    "settings": settings,
                    "steering": steering,
                    "detectors": detectors,
                    "normalization": normalization,
                },
            )
        )

        # TODO: this should not be here (in view)
        self._results_view = ScanResultsView(
            detectors, steering, settings, normalization
        )

        # TODO: widget shold not be updated like this
        self._widget.children = (*self._widget.children, self._results_view.widget)

        self._start_scan(detectors, steering, settings, normalization)

    def _on_stop_button(self, event):
        assert self._start_button is not None
        assert self._stop_button is not None

        self._start_button.disabled = False
        self._stop_button.disabled = True

        if self._handler:
            self._handler.stop()

    def _start_scan(self, detectors, steering, settings, normalization):
        if isinstance(steering, Collimator):
            self._handler = ScanCollimatorHandler(
                detectors, steering, settings, normalization
            )
        elif isinstance(steering, Magnet):
            self._handler = ScanMagnetHandler(
                detectors, steering, settings, normalization
            )
        else:
            raise ValueError(f"Unknown steering type: {type(steering)}")

        # TODO: not sure if handler is supposed to be spawned in a view class
        self._handler.add_observer(self._results_view)
        self._handler.start()

    def _add_observers_to_children(self):
        for observer in self.observers:
            self._add_observer_to_children(observer)

    def _add_observer_to_children(self, observer):
        if self._detectors_view:
            self._detectors_view.add_observer(observer)
        if self._steerings_view:
            self._steerings_view.add_observer(observer)
        if self._settings_view:
            self._settings_view.add_observer(observer)
        if self._normalizations_view:
            self._normalizations_view.add_observer(observer)

    def add_observer(self, observer):
        super().add_observer(observer)
        self._add_observer_to_children(observer)

    def refresh(self, action=None):
        assert self._detectors_view is not None
        assert self._steerings_view is not None
        assert self._settings_view is not None
        assert self._normalizations_view is not None

        self._detectors_view.refresh()
        self._steerings_view.refresh()
        self._settings_view.refresh()
        self._normalizations_view.refresh()

    def update(self, action):
        assert self._settings_view is not None
        assert self._steerings_view is not None

        if action is None:
            return

        name, _ = action
        if name == "steering selection":
            self._settings_view.steering = self._steerings_view.selected_steering
