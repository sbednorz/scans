import ipywidgets
import time
import datetime
import threading
import pandas as pd

from .view import View
from ..models import Magnet, AcquisitionStatus
from ..cesar import MAGNET_NAMES


class MagnetsView(View):
    # TODO: refactor to DeviceView and inherit from it

    def __init__(self, magnets=None, observers=None):
        super().__init__(observers)
        self._steerings = (
            magnets if magnets is not None else [Magnet(name) for name in MAGNET_NAMES]
        )
        self._df = self._build_df()
        self._running = False
        self._thread = threading.Thread(target=self._thread_loop)

        self._init_widget()
        self.start()

    def _init_widget(self):
        self._output = self._build_output_df()

        self._time_widget = self._build_time_widget()

        self._widget = ipywidgets.VBox()
        self.refresh()

    def _get_time_str(self):
        ts = time.time()
        date = datetime.datetime.fromtimestamp(ts)
        return str(date)

    def _build_time_widget(self):
        time = self._get_time_str()
        txt = f"Last update: {time}"
        return ipywidgets.Label(txt)

    def _build_output_df(self):
        output = ipywidgets.Output()
        output.append_display_data(self._df)
        return output

    def _build_df(self):
        data = {
            "Name": [steering.name for steering in self.steerings],
            "Read": [steering.current for steering in self.steerings],
            "BeamRef": [steering.ref for steering in self.steerings],
            "Max": [steering.max for steering in self.steerings],
            "Polarity": [0 for _ in self.steerings],
        }
        df = pd.DataFrame(data).set_index("Name")
        df = df.style.apply(MagnetsView._highlight_df, axis=1)
        return df

    @staticmethod
    def _highlight_df(x):
        # TODO: highlight if busy
        color = ""
        return [f"background-color: {color}" for _ in x]

    def _thread_loop(self):
        while self._running:
            self.refresh()
            time.sleep(1)

    def start(self):
        self._running = True
        self._thread.start()

    def stop(self):
        self._running = False
        self._thread.join()

    def refresh(self, action=None):
        self._time_widget = self._build_time_widget()

        for steering in self.steerings:
            steering.acquire(blocking=False)

        self._df = self._build_df()
        self._output = self._build_output_df()

        self._widget.children = [
            self._time_widget,
            self._output,
        ]

    @property
    def steerings(self):
        return self._steerings
