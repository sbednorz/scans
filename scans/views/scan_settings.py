import ipywidgets

from ..models import Steering, Collimator, Magnet
from .view import View
from .view_utils import ViewUtils
from .scan_settings_collimator import ScanSettingsCollimatorView
from .scan_settings_magnet import ScanSettingsMagnetView


class ScanSettingsView(View):
    LABEL_TEXT = "Scan settings"
    NO_STEERING_TEXT = "Select a steering device first."

    def __init__(self, observers=None, steering=None):
        super().__init__(observers)
        self._steering = None
        self._settings_view = None
        self._init_widget()
        self.steering = steering

    def _init_widget(self):
        self._steering_settings = ipywidgets.VBox(self._build_steering_settings())

        self._widget = ipywidgets.VBox(
            [
                ViewUtils.build_label(ScanSettingsView.LABEL_TEXT),
                self._steering_settings,
            ]
        )

    def _build_steering_settings(self):
        if self._steering is None:
            return self._build_no_steering_settings()
        elif isinstance(self._steering, Collimator):
            return self._build_collimator_settings()
        elif isinstance(self._steering, Magnet):
            return self._build_magnet_settings()
        else:
            raise NotImplementedError()

    def _build_no_steering_settings(self):
        self._settings_view = None
        return [ipywidgets.Label(ScanSettingsView.NO_STEERING_TEXT)]

    def _build_collimator_settings(self):
        self._settings_view = ScanSettingsCollimatorView(self.steering, self.observers)
        return [self._settings_view.widget]

    def _build_magnet_settings(self):
        self._settings_view = ScanSettingsMagnetView(self.steering, self.observers)
        return [self._settings_view.widget]

    def refresh(self):
        self._steering_settings.children = self._build_steering_settings()

    @property
    def settings(self):
        if self._settings_view:
            return self._settings_view.settings
        else:
            return None

    @property
    def steering(self):
        return self._steering

    @steering.setter
    def steering(self, value):
        assert isinstance(value, Steering) or value is None

        self._steering = value
        self.refresh()
