import ipywidgets

from ..models import Magnet, MagnetSettings
from .view import View
from .view_utils import ViewUtils


class ScanSettingsMagnetView(View):
    def __init__(self, magnet, observers=None):
        super().__init__(observers)
        self._settings = MagnetSettings()
        self._widget = ipywidgets.HBox([])
        self.magnet = magnet
        self._init_widget()

    def _init_widget(self):
        self._center_start_input = ipywidgets.BoundedFloatText(
            value=self.settings.start,
            min=self.steering.min,
            max=self.steering.max,
            step=None,
            description="Center start:",
        )

        self._center_end_input = ipywidgets.BoundedFloatText(
            value=self.settings.end,
            min=self.steering.min,
            max=self.steering.max,
            step=None,
            description="Center end:",
        )

        self._steps_input = ipywidgets.BoundedIntText(
            value=self.settings.step,
            min=self.settings.min_step,
            max=self.settings.max_step,
            description="Steps:",
        )

        self._tolerance_input = ipywidgets.BoundedFloatText(
            value=self.settings.tolerance,
            min=0,
            max=100,  # TODO: remove hardcoded value
            step=None,
            description="Tolerance:",
        )

        self._center_start_input.observe(self._update_model, names="value")
        self._center_end_input.observe(self._update_model, names="value")
        self._steps_input.observe(self._update_model, names="value")
        self._tolerance_input.observe(self._update_model, names="value")

        units = self.steering.units_name

        self._widget.children = [
            ipywidgets.VBox(
                [
                    ipywidgets.HBox(
                        [
                            self._center_start_input,
                            ipywidgets.Label(
                                f"Min: {ViewUtils.format_number(self.steering.min, units)}"
                            ),
                        ]
                    ),
                    ipywidgets.HBox(
                        [
                            self._center_end_input,
                            ipywidgets.Label(
                                f"Max: {ViewUtils.format_number(self.steering.max, units)}"
                            ),
                        ]
                    ),
                    self._steps_input,
                    self._tolerance_input,
                ],
                layout=ipywidgets.Layout(width="60%"),
            ),
            ipywidgets.VBox(
                [
                    ipywidgets.Label(
                        f"Ref.: {ViewUtils.format_number(self.steering.ref, units)}"
                    ),
                    ipywidgets.Label(
                        f"Current: {ViewUtils.format_number(self.steering.current, units)}"
                    ),
                ]
            ),
        ]

    def _update_model(self, action=None):
        self._settings.start = self._center_start_input.value
        self._settings.end = self._center_end_input.value
        self._settings.step = self._steps_input.value
        self._settings.tolerance = self._tolerance_input.value

    def refresh(self):
        self._init_widget()  # TODO: refreshing should be lighter
        self._update_model()

    @property
    def settings(self):
        return self._settings

    @property
    def steering(self):
        return self._magnet

    @property
    def magnet(self):
        return self._magnet

    @magnet.setter
    def magnet(self, value):
        assert isinstance(value, Magnet)

        self._magnet = value
        self.refresh()
