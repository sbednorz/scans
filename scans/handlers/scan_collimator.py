from .scan_handler import ScanHandler, ScanHandlerState
from ..models import Collimator, CollimatorSettings


class ScanCollimatorHandler(ScanHandler):
    def __init__(self, detectors, collimator, settings, normalization, observers=None):
        super().__init__(detectors, collimator, settings, normalization, observers)
        self._setter = None

    def _set_steering(self, collimator):
        assert isinstance(collimator, Collimator)

        if not collimator.correct:
            raise ValueError("Collimator is in invalid sate.")

        super()._set_steering(collimator)

    def _set_settings(self, settings):
        assert isinstance(settings, CollimatorSettings)

        if not settings.correct:
            raise ValueError("Collimator settings are in invalid sate.")

        self._settings = settings
        super()._set_settings(settings)

    def _get_req_motor_poses(self, step_pos):
        return step_pos - self._settings.slit / 2, step_pos + self._settings.slit / 2

    def _steer(self, step_pos):
        self._set_state(ScanHandlerState.STEERING)

        req_pos1, req_pos2 = self._get_req_motor_poses(step_pos)

        self._setter = self.collimator.set_poses(
            req_pos1, req_pos2, self.settings.tolerance
        )
        self._setter.wait()  # blocking but can be stopped by setter.stop()

        return True

    def _stop_steer(self):
        self._setter.stop()

    @property
    def collimator(self):
        assert isinstance(self._steering, Collimator)
        return self._steering
