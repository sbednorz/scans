from .scan_handler import ScanHandler, ScanHandlerState
from ..models import Magnet, MagnetSettings


class ScanMagnetHandler(ScanHandler):
    def __init__(self, detectors, magnet, settings, normalization, observers=None):
        super().__init__(detectors, magnet, settings, normalization, observers)
        self._setter = None

    def _set_steering(self, magnet):
        assert isinstance(magnet, Magnet)

        if not magnet.correct:
            raise ValueError("Magnet is in invalid sate.")

        super()._set_steering(magnet)

    def _set_settings(self, settings):
        assert isinstance(settings, MagnetSettings)

        if not settings.correct:
            raise ValueError("Magnet settings are in invalid sate.")

        self._settings = settings
        super()._set_settings(settings)

    def _steer(self, step_current):
        self._set_state(ScanHandlerState.STEERING)

        self._setter = self.magnet.set_current(step_current, self.settings.tolerance)
        self._setter.wait()  # blocking but can be stopped by setter.stop()

        return True

    def _stop_steer(self):
        self._setter.stop()

    @property
    def magnet(self):
        assert isinstance(self._steering, Magnet)
        return self._steering
