from abc import ABC, abstractmethod
from enum import Enum
import time
import threading
from ..models import (
    Detector,
    Steering,
    SteeringSettings,
    DetectorsGroupSubscriber,
    Normalization,
)


class ScanHandlerState(Enum):
    INITIALIZING = 1
    STEERING = 2
    MEASURING = 3
    FINISHED = 4


class ScanHandler(ABC):
    def __init__(
        self,
        detectors,
        steering,
        settings,
        normalization,
        observers=None,
    ):
        self._sub = None
        self._set_observers(observers)
        self._set_state(ScanHandlerState.INITIALIZING)
        self._set_detectors(detectors)
        self._set_steering(steering)
        self._set_settings(settings)
        self._set_normalization(normalization)
        # Starting thread is required if we want to interact
        # with Jupyter in the meantime (eg click Stop button)
        self._set_thread()
        self._exc_info = None

    def _is_set(self):
        return (
            self.detectors is not None
            and len(self.detectors) > 0
            and self.steering is not None
            and self.settings is not None
        )

    def _is_correct(self):
        return self._is_set()

    def _set_detectors(self, values):
        assert all(isinstance(value, Detector) for value in values)
        self._detectors = values

    def _set_steering(self, value):
        assert isinstance(value, Steering)
        self._steering = value

    def _set_settings(self, value):
        assert isinstance(value, SteeringSettings)
        self._settings = value

    def _set_normalization(self, normalization):
        assert isinstance(normalization, Normalization)
        self._normalization = normalization

    def _set_observers(self, observers):
        self._observers = [] if observers is None else observers

    def _set_thread(self):
        self._thread = threading.Thread(
            target=self._start, name=self._get_thread_name()
        )

    def _set_state(self, state):
        self._state = state
        self._notify_observers(("state", state))

    def _notify_observers(self, *args, **kwargs):
        for observer in self.observers:
            observer.update(*args, **kwargs)

    def _get_thread_name(self):
        return f"Scan-{hash(self)}"

    @abstractmethod
    def _steer(self, step_value):
        pass

    @abstractmethod
    def _stop_steer(self):
        pass

    def _stop_measure(self):
        # TODO: here we wait unitl _sub will be initialized,
        # almost never happens but is possible (look at _measure method)
        if self._sub is None:
            return

        # stops blocking acquisition
        self._sub.stop()

    def _measure(self):
        self._set_state(ScanHandlerState.MEASURING)
        dets = self.detectors

        if not self.normalization.no_normalization:
            dets = dets + [self.normalization.detector]

        self._sub = DetectorsGroupSubscriber(dets)
        # this is blocking, but we can call stop() in the meantime on this object
        self._sub.acquire_wait()
        self._exc_info = self._sub._exc_info

        if self.normalization.no_normalization:
            normalization_read = None
        else:
            normalization_read = self.normalization.detector.count

        # TODO: normalizing here is not very explicit
        # TODO: its not clear that normalization detector is already acquired here
        # TODO: this notification system is simple but not very unified
        self._notify_observers(
            (
                "result",
                (
                    {
                        det: self.normalization.normalize(det.count)
                        for det in self.detectors
                    },
                    normalization_read,
                ),
            )
        )
        return True

    def _start(self):
        step_values = self.settings.get_step_poses()
        for step_value in step_values:
            if not self.step(step_value):
                return False

        self._set_state(ScanHandlerState.FINISHED)
        return True

    def step(self, step_value):
        if not self._steer(step_value):
            return False

        if self._state == ScanHandlerState.FINISHED:
            return False

        if not self._measure():
            return False

        if self._state == ScanHandlerState.FINISHED:
            return False

        return True

    def start(self):
        # self._start()
        self._thread.start()

    def stop(self):
        if self._state == ScanHandlerState.INITIALIZING:
            pass
        elif self._state == ScanHandlerState.STEERING:
            self._stop_steer()
        elif self._state == ScanHandlerState.MEASURING:
            self._stop_measure()

        self._set_state(ScanHandlerState.FINISHED)

    def add_observer(self, observer):
        self._observers.append(observer)

    @property
    def detectors(self):
        return self._detectors

    @property
    def steering(self):
        return self._steering

    @property
    def settings(self):
        return self._settings

    @property
    def normalization(self):
        return self._normalization

    @property
    def observers(self):
        return self._observers
