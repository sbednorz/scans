from ..models import Normalizations, Detector, Collimator, Magnet

DEFAULT_AVAILABLE_NORMALIZATIONS = Normalizations.build_all()

DEFAULT_AVAILABLE_DETECTORS = [
    Detector("XSCI.021.131"),
    Detector("XSCI.021.386"),
    Detector("XSCI.021.461"),
]

DEFAULT_AVAILABLE_STEERINGS = [
    Collimator("XCSV.021.649"),
    Magnet("QUAD.022.526"),
]
