"""Due to the lack of easy-accessible CESAR API, list of available 
devices for scans are stored in this submodule.

"""


from .detector_names import DETECTORS_NAMES
from .steering_names import COLLIMATOR_NAMES, MAGNET_NAMES, TAX_NAMES
from .defaults import (
    DEFAULT_AVAILABLE_NORMALIZATIONS,
    DEFAULT_AVAILABLE_DETECTORS,
    DEFAULT_AVAILABLE_STEERINGS,
)
