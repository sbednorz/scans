from enum import Enum


class CollimatorMotorState(Enum):
    IDLE = 0
    MOVING = 1
    NEEDS_CORRECTION = 2
    READY = 3
    ERROR = 4
