from .device import Device


class Detector(Device):
    """Represents a detector device."""

    PROPERTY = "count"

    def __init__(self, name):
        super().__init__(name)
        self._count = None

    @property
    def operational(self):
        # TODO: not sure if used somewhere.
        # If yes, replace with Operational class
        return True

    @property
    def count(self):
        return self._count
