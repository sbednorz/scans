import sys
import time

from .steering import Steering
from .magnet_current_setter import MagnetCurrentSetter
from .subscribers import DeviceGetter, DeviceSubscriber
from .callbacks import SubscriberCallback


class Magnet(Steering):
    """Represents a magnet device with two motors."""

    CURRENT_REF_PROPERTY_NAME = "currentReference"
    CURRENT_PROPERTY_NAME = "current"
    UNITS_NAME = "Amp"

    def __init__(self, name):
        super().__init__(name)
        self.clear()
        self.acquire_without_current()

        self._acquiring_running = False

    def __str__(self):
        return (
            f"min: {self.min}\n"
            f"max: {self.max}\n"
            f"current: {self.current}\n"
            f"ref: {self.ref}\n"
        )

    def __repr__(self):
        return str(self)

    def _acquire_min(self):
        return float("nan")

    def _acquire_max(self):
        return float("nan")

    def _acquire_current(self):
        # TODO: This logic should be moved to the class which will be
        # responsilbe for getting values. See experimental PyJapecContext class.
        class _callback(SubscriberCallback):
            def __init__(self):
                super().__init__()
                self.value = None

            def callback(self, _, value, *args, **kwargs):
                self.value = value

        cb = _callback()
        sub = DeviceSubscriber(
            self,
            Magnet.CURRENT_PROPERTY_NAME,
            skip_first_update=True,
            max_count=1,
            observers=[cb],
        )
        sub.start()

        TIMEOUT = time.time() + 70.0
        while sub.running and time.time() < TIMEOUT:
            time.sleep(0.01)

        value = cb.value
        if value is None:
            print(
                "Timeout hit while waiting for subscribed parameter notification.",
                file=sys.stderr,
            )
            sub.stop()
            return float("inf")

        return cb.value

    def _acquire_current_in_background(self):
        # TODO: This logic should be moved to the class which will be
        # responsilbe for getting values. See experimental PyJapecContext class.

        if self._acquiring_running:
            return

        self._acquiring_running = True

        class _callback(SubscriberCallback):
            def __init__(self, magnet):
                super().__init__()
                self.magnet = magnet

            def callback(self, _, value, *args, **kwargs):
                # TODO: not not override values like this
                self.magnet._current = value
                self.magnet._acquiring_running = False

        cb = _callback(self)
        sub = DeviceSubscriber(
            self,
            Magnet.CURRENT_PROPERTY_NAME,
            skip_first_update=True,
            max_count=1,
            observers=[cb],
        )
        sub.start()

    def _acquire_ref(self):
        return float(DeviceGetter(self, Magnet.CURRENT_REF_PROPERTY_NAME).get())

    def _is_set(self):
        return (
            self._min is not None
            and self._max is not None
            and (self._current is None or self._min <= self._current <= self._max)
            and self._ref is not None
        )

    def _is_correct(self):
        return self._is_set()

    def clear(self):
        self._min = None
        self._max = None
        self._current = None
        self._ref = None

    def acquire_without_current(self):
        self._min = self._acquire_min()
        self._max = self._acquire_max()
        self._ref = self._acquire_ref()

    def acquire(self, blocking=True):
        if blocking:
            self._current = self._acquire_current()
        else:
            self._acquire_current_in_background()

        self.acquire_without_current()

    def set_current(self, req_current, tolerance):
        assert isinstance(req_current, (float, int))
        assert isinstance(tolerance, (float, int))

        setter = MagnetCurrentSetter(self, req_current, tolerance)
        setter.launch()

        return setter

    @property
    def correct(self):
        return self._is_correct()

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def current(self):
        return self._current

    @property
    def ref(self):
        return self._ref

    @property
    def units_name(self):
        return Magnet.UNITS_NAME
