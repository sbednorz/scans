from abc import ABC


class SteeringSettings(ABC):
    """Represents steering scan settings which are common for all steering devices."""

    def __init__(self):
        self.clear()

    def clear(self):
        self._start = 0.0
        self._end = 0.0
        self._step = 0.0
        self._tolerance = 0.0

    @staticmethod
    def get_step_size(first_value, last_value, steps_cnt):
        return abs(last_value - first_value) / (steps_cnt - 1)

    def get_step_poses(self):
        steps_cnt = int(self.step)
        first_value = self.start
        last_value = self.end
        step_size = SteeringSettings.get_step_size(first_value, last_value, steps_cnt)

        values = [first_value]
        for _ in range(1, steps_cnt):
            if last_value > first_value:
                values.append(values[-1] + step_size)
            else:
                values.append(values[-1] - step_size)

        return values

    @property
    def min_step(self):
        return 2

    @property
    def max_step(self):
        return 100

    @property
    def start(self):
        return self._start

    @property
    def end(self):
        return self._end

    @property
    def step(self):
        return self._step

    @property
    def tolerance(self):
        return self._tolerance

    @start.setter
    def start(self, value):
        assert isinstance(value, (float, int))
        self._start = value

    @end.setter
    def end(self, value):
        assert isinstance(value, (float, int))
        self._end = value

    @step.setter
    def step(self, value):
        assert isinstance(value, (float, int))
        assert abs(value) >= 2
        self._step = value

    @tolerance.setter
    def tolerance(self, value):
        assert isinstance(value, (float, int))
        assert value >= 0
        self._tolerance = value
