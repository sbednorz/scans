import ipywidgets
import pandas as pd
from plotly import graph_objs as go
from IPython.display import display
from abc import ABC, abstractmethod


class SubscriberCallback(ABC):
    """Represents a callback which is called when a device property is updated."""

    @abstractmethod
    def callback(self, *args, **kwargs):
        pass


class SubscriberEcho(SubscriberCallback):
    """Callback which prints the updates using a Jupyter widget."""

    def __init__(self):
        self._data = []
        self._output_widget = ipywidgets.Output()
        display(self._output_widget)

    def callback(self, *args, **kwargs):
        a = [str(arg) for arg in args]  # + [str(kwarg) for kwarg in kwargs]
        msg = f"{a}"

        self._data.append(msg)

        # WORKAROUND: updating ipywidgets.Output in the background using
        # context operator (and print) or the Output.append_stdout method does
        # not work correctly. In an undefined way updates are lost in
        # vscode notebooks and jupyter lab. The problem does not occur in
        # jupyter notebooks. Cleaning widget's outputs solves this issue.
        # WORKAROUND: cleaning widget's outputs by Output.clear_output()
        # causes race condition when multiple threads are involved.
        # Overriding outputs solved this problem.
        self._output_widget.outputs = ()

        self._output_widget.append_stdout(self._data)


class SubscriberPlot(SubscriberCallback):
    """Callback which plots the updates with plotly (supported by Jupyter)."""

    def __init__(self):
        self._df = pd.DataFrame(columns=["Reception time", "Device", "Value"])
        self._fig = go.FigureWidget(layout=go.Layout(width=1200, height=600))
        self._fig.add_trace(
            go.Scatter(
                x=self._df["Reception time"], y=self._df["Value"], mode="markers"
            )
        )
        display(self._fig)

    def callback(self, *args, **kwargs):
        if len(args) != 3:
            return None

        timestamp, device, value, headers = pd.Timestamp.now(), *args
        row = pd.DataFrame([[timestamp, device, value]], columns=self._df.columns)

        self._df = pd.concat([self._df, row], ignore_index=True)

        self._fig.data[0].x = self._df.iloc[:, 0]
        self._fig.data[0].y = self._df.iloc[:, 2]

        return self._df


class SubscriberDf(SubscriberCallback):
    """Callback which renders a pandas dataframe with the updates (supported by Jupyter)."""

    def __init__(self):
        self._df = pd.DataFrame(columns=["Reception time", "Device", "Value"])
        self._output_widget = ipywidgets.Output()
        self._output_widget.append_display_data(self._df)
        display(self._output_widget)

    def callback(self, *args, **kwargs):
        if len(args) != 3:
            return None

        timestamp, device, value, headers = pd.Timestamp.now(), *args
        row = pd.DataFrame([[timestamp, device, value]], columns=self._df.columns)

        self._df = pd.concat([self._df, row], ignore_index=True)

        self._output_widget.outputs = ()  # workaround because clear_output() causes race condition
        self._output_widget.append_display_data(self._df.tail(10))

        return self._df
