import time
import threading
import sys

from .detector import Detector
from .callbacks import SubscriberCallback
from .subscribers import DeviceSubscriber


class DetectorsGroupSubscriber:
    """Allows to subscribe for a one update to a parameters group.

    Can be blocking or non-blocking.

    TODO: This class should be refactored (see: JapcContext) and
    is probably unsafe and bad.
    """

    class _Callback(SubscriberCallback):
        def __init__(self):
            super().__init__()
            self.values = {}

        def callback(self, detector, value, *args, **kwargs):
            self.values[detector] = value

    def __init__(self, detectors):
        assert all(isinstance(d, Detector) for d in detectors)

        self._detectors = detectors
        self._callback = DetectorsGroupSubscriber._Callback()
        self._sub = DeviceSubscriber(
            self._detectors,
            Detector.PROPERTY,
            skip_first_update=True,
            max_count=1,
            observers=[self._callback],
        )
        self._running = False
        self._thread = None
        self._exc_info = None

    def _name_to_detectors(self, name):
        return [det for det in self._detectors if det.name == name]

    def acquire(self):
        """Starts the acquisition."""

        def _target(self):
            try:
                self._sub.start()

                TIMEOUT = time.time() + 90.0
                while self._sub.running and time.time() < TIMEOUT:
                    time.sleep(0.01)

                # acquisition has been stopped
                if not self._running:
                    self._sub.stop()
                    return

                values = self._callback.values
                if not values:
                    print(
                        "Timeout hit while waiting for subscribed parameter notification.",
                        file=sys.stderr,
                    )
                    self._sub.stop()
                    return

                self._sub.stop()

                # TODO: do it in the right way
                for str, value in values.items():
                    name = str.split("/")[0]
                    detector = self._name_to_detectors(name)[0]
                    detector._count = value  # TODO: modifying "private" values without verification etc

                self._running = False
            except Exception:
                self._exc_info = sys.exc_info()

        self._running = True
        self._thread = threading.Thread(target=_target, args=(self,))
        self._thread.start()

    def wait(self):
        """Waits until the acquisition is finished."""
        if self._thread is None:
            return

        self._thread.join()
        self._thread = None

    def acquire_wait(self):
        """Starts the acquisition and waits until it is finished."""
        self.acquire()
        self.wait()

    def stop(self):
        """Stops the acquisition immediately."""
        if not self._running:
            return False

        if self._thread is None:
            return False

        self._running = False
        self._sub.stop()
        self._thread.join()
        self._thread = None
        return True
