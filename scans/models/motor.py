from .device import Device, AcquisitionStatus
from .subscribers import DeviceGetter, DeviceSetter


class Motor(Device):
    """Represents a single motor device used by a collimator."""

    def __init__(self, name):
        super().__init__(name)
        self.clear()
        self.acquire()

    def __str__(self):
        return f"pos: {self.pos}\n" f"min: {self.min}\n" f"max: {self.max}\n"

    def __repr__(self):
        return self.name

    def _acquire_pos(self):
        return float(DeviceGetter(self, "Acquisition#position").get())

    def _acquire_min(self):
        return float(DeviceGetter(self, "Acquisition#position_min").get())

    def _acquire_max(self):
        return float(DeviceGetter(self, "Acquisition#position_max").get())

    def _acquire_pos_status(self):
        # TODO: add support for multiple values in AcquisitionStatus e.g.
        # 48 = 32 + 16 = TIMEOUT + BUSY
        return AcquisitionStatus(
            DeviceGetter(self, "Acquisition#position_status").get()
        )

    def _is_set(self):
        return self._min is not None and self._max is not None and self._pos is not None

    def _is_valid(self):
        assert isinstance(self.min, float)
        assert isinstance(self.pos, float)
        assert isinstance(self.max, float)

        return self.min > self.pos or self.pos > self.max

    def _is_correct(self):
        return self._is_set() and self._is_valid()

    def clear(self):
        self._pos = None
        self._min = None
        self._max = None
        self._pos_status = None

    def acquire(self):
        self._pos = self._acquire_pos()
        self._min = self._acquire_min()
        self._max = self._acquire_max()
        self._pos_status = self._acquire_pos_status()

    def set_pos(self, new_pos):
        setter = DeviceSetter(self, "Setting#position")
        setter.set(new_pos)

    @property
    def correct(self):
        return self._is_correct()

    @property
    def pos(self):
        return self._pos

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def pos_status(self):
        return self._pos_status
