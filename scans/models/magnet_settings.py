from .steering_settings import SteeringSettings


class MagnetSettings(SteeringSettings):
    """Represents magnet scan settings. Based on CESAR scans GUI."""

    DEFAULT_START = 0
    DEFAULT_END = 0
    DEFAULT_STEPS = 0
    DEFAULT_TOLERANCE = 0.4

    def __init__(self):
        self.clear()

    def __str__(self):
        return (
            f"start: {self.start}\n"
            f"end: {self.end}\n"
            f"step: {self.step}\n"
            f"tolerance: {self.tolerance}\n"
        )

    def __repr__(self):
        return str(self)

    def _is_set(self):
        """Verifies if the magnet scan settings are set."""
        return (
            self._start is not None
            and self._end is not None
            and self._step is not None
            and self._tolerance is not None
        )

    def _is_valid(self):
        start = self.start
        end = self.end
        step = self.step
        tolerance = self.tolerance
        return (
            tolerance > 0 and start < end and step <= (end - start) and step > 0.0
        ) or (tolerance > 0 and start > end and step <= (start - end) and step < 0.0)

    def _is_correct(self):
        """Verifies if the magnet scan settings are valid."""
        return self._is_set() and self._is_valid()

    def clear(self):
        """Deinitialize the magnet scan settings."""
        self._start = MagnetSettings.DEFAULT_START
        self._end = MagnetSettings.DEFAULT_END
        self._step = MagnetSettings.DEFAULT_STEPS
        self._tolerance = MagnetSettings.DEFAULT_TOLERANCE

    @property
    def correct(self):
        return self._is_correct()
