import pyjapc
import logging
from threading import Lock

DEFAULT_SELECTOR = "SPS.USER.ALL"

# TODO: Multiple _japc instances for experimenting and development purposes
# Should be replaced with only one, which is used by all devices.
#
# See more in the PyJapcContext class.
_japc = pyjapc.PyJapc(
    selector="SPS.USER.ALL",
    noSet=True,
    incaAcceleratorName=None,
    logLevel=logging.ERROR,
)
_japc_get = pyjapc.PyJapc(
    selector="", noSet=True, incaAcceleratorName=None, logLevel=logging.ERROR
)
_japc_set = pyjapc.PyJapc(
    selector="", noSet=False, incaAcceleratorName=None, logLevel=logging.ERROR
)
_japc_noset = pyjapc.PyJapc(
    selector="", noSet=True, incaAcceleratorName=None, logLevel=logging.ERROR
)


class DeviceSetter:
    """PyJapc wrapper to set a device property.

    Should be replaces with something better. See PyJapcContext class.
    """

    def __init__(self, device, property, no_set=False):
        self._device = device
        self._property = property

        if no_set:
            self._japc = _japc_noset
        else:
            self._japc = _japc_set

    def set(self, value):
        param = f"{self._device._name}/{self._property}"
        self._japc.setParam(param, value, checkDims=False)


class DeviceGetter:
    """PyJapc wrapper to get a device property.

    Should be replaces with something better. See PyJapcContext class.
    """

    def __init__(self, device, property, observers=None):
        self._device = device
        self._property = property
        self._observers = [] if observers is None else observers

        self._japc = _japc_get

    def get(self):
        str = f"{self._device._name}/{self._property}"
        value = self._japc.getParam(str)

        for observer in self._observers:
            observer.callback(self._property, value)

        return value


class DeviceSubscriber:
    """PyJapc wrapper to subscribe to a device property and pass
    the updates to the SubscriberCallback oberservers.

    Should be replaces with something better. See PyJapcContext class.
    """

    def __init__(
        self,
        devices,
        property,
        observers=None,
        skip_first_update=True,
        override_selector=None,
        max_count=float("inf"),
    ):
        if isinstance(devices, list):
            self._devices = devices
        else:
            self._devices = [devices]
        self._property = property
        self._observers = [] if observers is None else observers
        self._skip_first_update = skip_first_update
        self._override_selector = override_selector
        self._max_count = max_count
        self._count = 0
        self._cb_lock = Lock()

        self._running = False
        self._japc = None

        self._japc = _japc

    @property
    def running(self):
        return self._running

    @property
    def count(self):
        return self._count

    def _callback(self, properties, values, *args):
        with self._cb_lock:
            if not self._running:
                return

            # TODO: make this "if" prettier ant in other places too
            if isinstance(properties, list):
                for i in range(len(properties)):
                    property = properties[i]
                    value = values[i]
                    header = args[0][i]

                    if self._skip_first_update and header["isFirstUpdate"]:
                        return

                    for observer in self._observers:
                        observer.callback(property, value, header)
            else:
                if self._skip_first_update and args[0]["isFirstUpdate"]:
                    return

                for observer in self._observers:
                    observer.callback(properties, values, *args)

            self._count += 1

            if self._count >= self._max_count:
                self.stop()

    def _callback_exception(self, *args, **kwargs):
        # TODO: handle errors
        pass
        print(f"Exception in subscriber callback: {args} {kwargs}", flush=True)
        # raise Exception(f"Exception in subscriber callback: {args} {kwargs}")

    def _devices_to_parameters(self):
        if len(self._devices) == 1:
            return f"{self._devices[0].name}/{self._property}"
        else:
            return [f"{device.name}/{self._property}" for device in self._devices]

    def start(self):
        assert isinstance(self._japc, pyjapc.PyJapc)
        if self._running:
            raise Exception("Already running")

        self._running = True
        self._count = 0

        # TODO: make it prettier. override_selector cannot be passed
        # to _japc as None, because its interpreted as an empty selector then
        if self._override_selector is None:
            self._japc.subscribeParam(
                parameterName=self._devices_to_parameters(),
                onValueReceived=self._callback,
                onException=self._callback_exception,
                getHeader=True,
            )
        else:
            self._japc.subscribeParam(
                parameterName=self._devices_to_parameters(),
                onValueReceived=self._callback,
                onException=self._callback_exception,
                timingSelectorOverride=self._override_selector,
                getHeader=True,
            )
        self._japc.startSubscriptions()

    def stop(self):
        if not self._running or self._japc is None:
            return

        self._running = False

        parameters = self._devices_to_parameters()
        if len(parameters) == 1:
            self._japc.stopSubscriptions(parameters[0])
            self._japc.clearSubscriptions(parameters[0])
        else:
            for parameter in parameters:
                self._japc.stopSubscriptions(parameter)
                self._japc.clearSubscriptions(parameter)
