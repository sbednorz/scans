import pyjapc
import logging


# TODO: Future class which can solve the problems with
# low-level device acquisitions and higher-level logics.
# Usages:
#   [x] simple get/set
#   [x] one pyjapc instance for all devices
#   [ ] devices models can acquire values using this object
#   [ ] subscribe and return 1 value
#       [ ] in background
#       [ ] blocking
#       [ ] can be stopped at any time
#       [ ] for multiple devices
#   [ ] subscribe
#       [ ] in background
#       [ ] blocking
#       [ ] can be stopped at any time
#       [ ] for multiple devices
#   [ ] subscribpion can update the model if flag is set
class PyJapcContext:
    DEFAULT_SELECTOR = "SPS.USER.ALL"  # Should be always used
    EMPTY_SELECTOR = ""  # Some devices do not support other selectors than empty
    DEFAULT_LOG_LEVEL = logging.ERROR

    _pyjapc = None

    @classmethod
    def get(cls, device_name, property, override_selector=EMPTY_SELECTOR):
        return cls.pyjapc().getParam(
            f"{device_name}/{property}", timingSelectorOverride=override_selector
        )

    @classmethod
    def set(cls, device_name, property, value, override_selector=EMPTY_SELECTOR):
        cls.pyjapc().setParam(
            f"{device_name}/{property}",
            value,
            checkDims=False,
            timingSelectorOverride=override_selector,
        )

    @classmethod
    def subscribe(
        cls,
        devices,
        property,
        callback=None,
        override_selector=DEFAULT_SELECTOR,
        skip_first_update=True,
        separate_threads=False,
        count=None,
        update_model=False,
    ):
        # validate and prepare callback
        if isinstance(callback, SubscriberCallback):
            callback = callback.callback
        elif callable(callback):
            callback = callback
        else:
            raise ValueError(f"Unknown callback type: {type(callback)}")

        # validate and prepare params and devices
        if isinstance(devices, (list, tuple)):
            params = [f"{device}/{property}" for device in devices]
        elif isinstance(devices, Device):
            params = f"{devices}/{property}"
            devices = [devices]
        else:
            raise ValueError(f"Unknown devices type: {type(devices)}")

        # init separete thread if needed

        # start subscription
        cls.pyjapc().subscribeParam(
            params,
            lambda properties, values, *args: cls._callback_wrapper(
                properties,
                values,
                devices,
                callback,
                args,
                skip_first_update=skip_first_update,
                count=count,
                update_model=update_model,
            ),
            timingSelectorOverride=override_selector,
        )

    @classmethod
    def subscribe_once(
        cls,
        devices,
        property,
        callback=None,
        override_selector=DEFAULT_SELECTOR,
        skip_first_update=True,
        separate_threads=False,
        update_model=False,
    ):
        cls.subscribe(
            devices=devices,
            property=property,
            callback=callback,
            override_selector=override_selector,
            skip_first_update=skip_first_update,
            separate_threads=separate_threads,
            update_model=update_model,
            count=1,
        )

    @classmethod
    def _callback_wrapper(
        cls,
        properties,
        values,
        devices,
        callback,
        *args,
        skip_first_update=True,
        count=None,
        update_model=False,
    ):
        # if subscribing a single property
        if not isinstance(properties, list):
            properties = [properties]
            values = [values]
            args = [args]

        headers = args[0]

        # skip first update if needed
        if skip_first_update and any(header["isFirstUpdate"] for header in headers):
            return

        # TODO: update model if needed

        # call callback
        for property, value, header, device in zip(
            properties, values, headers, devices
        ):
            callback(property, value, header, device)

        # TODO: check counter and stop if needed

        raise NotImplementedError("TODO: implement this")

    @classmethod
    def pyjapc(cls):
        if cls._pyjapc is None:
            cls._pyjapc = pyjapc.PyJapc(
                selector=cls.DEFAULT_SELECTOR,
                noSet=False,
                incaAcceleratorName=None,
                logLevel=cls.DEFAULT_LOG_LEVEL,
            )
        return cls._pyjapc
