from .subscribers import PyJapcContext
from enum import Enum

import sys


class DeviceStatus(Enum):
    UNKNOWN = 0
    OK = 1
    WARNING = 2
    ERROR = 3

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class DeviceMode(Enum):
    UNKNOWN = 0
    ON = 1
    OFF = 2
    STANDBY = 3
    INITIALIZING = 4

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class DeviceControl(Enum):
    UNKNOWN = 0
    REMOTE = 5
    LOCAL = 6

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class Fesa3DeviceStatus(Enum):
    INFO = 0
    WARNING = 1
    ERROR = 2
    CRITICAL = 3


class Operational:
    """
    Representing a device which can be checked for its operational status.
    Note that not all devices support this feature.
    """

    def _to_device_status(self, fesa3_status):
        status = Fesa3DeviceStatus(fesa3_status)
        if status == Fesa3DeviceStatus.INFO:
            return DeviceStatus.OK
        elif status == Fesa3DeviceStatus.WARNING:
            return DeviceStatus.WARNING
        elif status in [Fesa3DeviceStatus.ERROR, Fesa3DeviceStatus.CRITICAL]:
            return DeviceStatus.ERROR
        else:
            return DeviceStatus.UNKNOWN

    def _to_device_mode(self, mode):
        return DeviceMode(mode)

    def _to_device_control(self, control):
        return DeviceControl(control)

    @property
    def operational(self):
        """
        Returns a boolean indicating whether the device is operational or not.
        """
        try:
            res = PyJapcContext.get(self, "Status")
        except Exception as e:
            print(repr(e), file=sys.stderr)
            return

        assert "mode" in res
        assert "control" in res
        assert "status" in res

        mode = self._to_device_mode(res["mode"])
        control = self._to_device_control(res["control"])
        status = self._to_device_status(res["status"])

        if status not in [DeviceStatus.OK, DeviceStatus.WARNING]:
            return False

        if mode not in [DeviceMode.ON, DeviceMode.STANDBY]:
            return False

        return control == DeviceControl.REMOTE
