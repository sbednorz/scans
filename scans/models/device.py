import sys
from enum import Enum
from .subscribers import DeviceGetter


class Orientation(Enum):
    HORIZONTAL = "H"
    VERTICAL = "V"


class Fesa3DeviceStatus(Enum):
    """Ported from CESAR."""

    INFO = 0
    WARNING = 1
    ERROR = 2
    CRITICAL = 3


class DeviceStatus(Enum):
    """Ported from CESAR."""

    UNKNOWN = 0
    OK = 1
    WARNING = 2
    ERROR = 3

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class DeviceMode(Enum):
    """Ported from CESAR."""

    UNKNOWN = 0
    ON = 1
    OFF = 2
    STANDBY = 3
    INITIALIZING = 4

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class DeviceControl(Enum):
    """Ported from CESAR."""

    UNKNOWN = 0
    REMOTE = 5
    LOCAL = 6

    @classmethod
    def _missing_(cls, value):
        return cls.UNKNOWN


class AcquisitionStatus(Enum):
    """Ported from CESAR."""

    OK = 0
    NOT_OK = 1
    BAD_QUALITY = 2
    DIFFERENT_FROM_SETTING = 4
    OUT_OF_RANGE = 8
    BUSY = 16
    TIMEOUT = 32


class Device:
    """Represents a generic named device."""

    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    @property
    def operational(self):
        # TODO: define Operational class.
        try:
            res = DeviceGetter(self, "Status").get()
        except Exception as e:
            print(repr(e), file=sys.stderr)
            return None

        assert "mode" in res
        assert "control" in res
        assert "status" in res

        mode = self._to_device_mode(res["mode"])
        control = self._to_device_control(res["control"])
        status = self._to_device_status(res["status"])

        if status != DeviceStatus.OK and status != DeviceStatus.WARNING:
            return False

        if mode != DeviceMode.ON and mode != DeviceMode.STANDBY:
            return False

        if control != DeviceControl.REMOTE:
            return False

        return True

    def _to_device_status(self, fesa3_status):
        status = Fesa3DeviceStatus(fesa3_status)
        if status == Fesa3DeviceStatus.INFO:
            return DeviceStatus.OK
        elif status == Fesa3DeviceStatus.WARNING:
            return DeviceStatus.WARNING
        elif status == Fesa3DeviceStatus.ERROR:
            return DeviceStatus.ERROR
        elif status == Fesa3DeviceStatus.CRITICAL:
            return DeviceStatus.ERROR
        else:
            return DeviceStatus.UNKNOWN

    def _to_device_mode(self, mode):
        return DeviceMode(mode)

    def _to_device_control(self, control):
        return DeviceControl(control)
