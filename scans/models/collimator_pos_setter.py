import threading
import sys
import time

from .device import AcquisitionStatus
from .callbacks import SubscriberCallback
from .subscribers import DeviceSubscriber
from .collimator_motor_state import CollimatorMotorState


class CollimatorPosSetterCallback(SubscriberCallback):
    MAX_CORRECTIONS_CNT = 3

    def __init__(self, coll_pos_setter, motor, req_pos):
        self._coll_pos_setter = coll_pos_setter
        self._motor = motor
        self._req_pos = req_pos
        self._exc_info = None

        self._motor1_correction_cnt = 0
        self._motor2_correction_cnt = 0

    def callback(self, *args, **kwargs):
        try:
            self._motor.acquire()  # TODO: we subscribe to the param but we cant simply update the motor

            cur_pos = args[1]
            # in tolerance
            if abs(cur_pos - self._req_pos) < self._coll_pos_setter._tolerance:
                if self._motor is self._coll_pos_setter._motor1:
                    self._coll_pos_setter._motor1_state = CollimatorMotorState.READY
                    self._coll_pos_setter._motor1_sub.stop()
                elif self._motor is self._coll_pos_setter._motor2:
                    self._coll_pos_setter._motor2_state = CollimatorMotorState.READY
                    self._coll_pos_setter._motor2_sub.stop()
            # not in tolerance but status is ok
            elif self._motor.pos_status == AcquisitionStatus.OK:
                if self._motor is self._coll_pos_setter._motor1:
                    if self._motor1_correction_cnt < self.MAX_CORRECTIONS_CNT:
                        self._coll_pos_setter._motor1_state = (
                            CollimatorMotorState.NEEDS_CORRECTION
                        )
                        self._motor1_correction_cnt += 1
                    else:
                        self._coll_pos_setter._motor1_state = CollimatorMotorState.ERROR
                        self._coll_pos_setter._motor1_sub.stop()
                elif self._motor is self._coll_pos_setter._motor2:
                    if self._motor2_correction_cnt < self.MAX_CORRECTIONS_CNT:
                        self._coll_pos_setter._motor2_state = (
                            CollimatorMotorState.NEEDS_CORRECTION
                        )
                        self._motor2_correction_cnt += 1
                    else:
                        self._coll_pos_setter._motor2_state = CollimatorMotorState.ERROR
                        self._coll_pos_setter._motor2_sub.stop()

        except Exception as e:
            self._exc_info = sys.exc_info()

    @property
    def exc_info(self):
        return self._exc_info


class CollimatorPosSetter:
    # TODO: The current implementation is not perfect and
    # robust if it comes to thread safety. It should be reconsidered
    # and simplified in the future to avoid problems.

    # TODO: Refactor: move motor-related logic to MotorPosSetter.
    # Now the motors are controlled directly from the CollimatorPosSetter,
    # It would be better to move it to new MotorPosSetter class and use it here.

    # TODO: if JAW_TIME_MARGIN_MS=0, motors subscription spawning race condition (see CSR-1990)
    JAW_TIME_MARGIN_MS = 3000.0
    JAW_TIME_MARGIN_S = JAW_TIME_MARGIN_MS / 1000.0

    def __init__(self, collimator, req_pos1, req_pos2, tolerance):
        assert isinstance(req_pos1, (float, int))
        assert isinstance(req_pos2, (float, int))
        # TODO: assert isinstance(collimator, collimator.Collimator) avoiding circular imports
        assert isinstance(tolerance, (float, int))
        assert collimator.correct

        self._coll = collimator
        self._motor1 = self._coll.motor1
        self._motor2 = self._coll.motor2
        self._req_pos1 = req_pos1
        self._req_pos2 = req_pos2
        self._tolerance = tolerance

        self.clear()

    def clear(self):
        self._motor1_sub = None
        self._motor2_sub = None
        self._motor1_setter = None
        self._motor2_setter = None

        self._motor_thread1 = None
        self._motor_thread2 = None

        self._motor1_callback = None
        self._motor2_callback = None

        self._motor1_state = CollimatorMotorState.IDLE
        self._motor2_state = CollimatorMotorState.IDLE

    def launch(self):
        if not self._coll.operational:
            raise Exception("Collimator is not operational")

        if not self._are_req_poses_in_range():
            raise Exception("Request poses are not in range")

        if not self._is_minimum_gap_respected():
            raise Exception("Minimum gap is not respected")

        first_jaw_first = self._is_first_jaw_going_first()
        if not first_jaw_first:
            self._swap_motors()

        self._init_motor1_sub()
        self._init_motor2_sub()

        self._motor_thread1 = threading.Thread(
            target=self._launch_motor,
            args=[self._motor1, self._req_pos1, self._motor1_sub],
        )
        self._motor_thread2 = threading.Thread(
            target=self._launch_motor_delayed,
            args=[self._motor2, self._req_pos2, self._motor2_sub],
        )

        assert isinstance(self._motor1_sub, DeviceSubscriber)
        assert isinstance(self._motor2_sub, DeviceSubscriber)

        # TODO: handle bahviour when one motor is on the right position

        # set first motor
        self._motor_thread1.start()
        self._motor1.acquire()
        # self._motor1_sub.start()

        # set second motor
        self._motor_thread2.start()
        self._motor2.acquire()
        # self._motor2_sub.start()

        return self  # Future or sth

    def stop(self):
        if self._motor1_sub is not None:
            self._motor1_sub.stop()

        if self._motor2_sub is not None:
            self._motor2_sub.stop()

        self._motor1_state = CollimatorMotorState.ERROR  # TODO: maybe STOPPED or sth?

        self._motor2_state = CollimatorMotorState.ERROR

        if self._motor_thread1 is not None:
            self._motor_thread1.join()

        if self._motor_thread2 is not None:
            self._motor_thread2.join()

    def wait(self):
        while self.running:
            time.sleep(0.01)

    def _build_sub(self, motor, req_pos):
        cb = CollimatorPosSetterCallback(self, motor, req_pos)
        sub = DeviceSubscriber(
            motor,
            "Acquisition#position",  # TODO: unnecessary acquisition, we should get it from the motor model
            [cb],
            override_selector="",
        )
        return cb, sub

    def _init_motor1_sub(self):
        self._motor1_callback, self._motor1_sub = self._build_sub(
            self._motor1, self._req_pos1
        )

    def _init_motor2_sub(self):
        self._motor2_callback, self._motor2_sub = self._build_sub(
            self._motor2, self._req_pos2
        )

    def _swap_motors(self):
        self._motor1, self._motor2 = self._motor2, self._motor1
        self._req_pos1, self._req_pos2 = self._req_pos2, self._req_pos1
        self._motor1_sub, self._motor2_sub = self._motor2_sub, self._motor1_sub
        self._motor1_setter, self._motor2_setter = (
            self._motor2_setter,
            self._motor1_setter,
        )
        self._motor_thread1, self._motor_thread2 = (
            self._motor_thread2,
            self._motor_thread1,
        )
        self._motor1_callback, self._motor2_callback = (
            self._motor2_callback,
            self._motor1_callback,
        )
        self._motor1_state, self._motor2_state = self._motor2_state, self._motor1_state

    def _launch_motor(self, motor, req_pos, sub):
        if motor is self._motor1:
            self._motor1_state = CollimatorMotorState.MOVING
        elif motor is self._motor2:
            self._motor2_state = CollimatorMotorState.MOVING

        motor.set_pos(req_pos)

        # TODO: it prevents from setting motor in NEEDS_CORRECTION state if the sub
        # starts before the motor is set. Replace with sth better.
        time.sleep(0.1)

        sub.start()

        # TODO: refactor this while loop
        while self._motor1_state not in [
            CollimatorMotorState.READY,
            CollimatorMotorState.ERROR,
        ] or self._motor2_state not in [
            CollimatorMotorState.READY,
            CollimatorMotorState.ERROR,
        ]:
            time.sleep(0.01)
            if (
                motor is self._motor1
                and self._motor1_state == CollimatorMotorState.NEEDS_CORRECTION
            ):
                self._motor1_state = CollimatorMotorState.MOVING
                motor.set_pos(req_pos)
            elif (
                motor is self._motor2
                and self._motor2_state == CollimatorMotorState.NEEDS_CORRECTION
            ):
                self._motor2_state = CollimatorMotorState.MOVING
                motor.set_pos(req_pos)

    def _launch_motor_delayed(self, motor, req_pos, sub):
        # wait for the first motor to start moving
        time.sleep(self.JAW_TIME_MARGIN_S)

        # set the motor
        self._launch_motor(motor, req_pos, sub)

    def _is_finished(self):
        # TODO: its not used anywhere
        # update current positions
        return self._are_poses_reached()

    def _are_poses_reached(self):
        return self._is_pos1_in_tolerance() and self._is_pos2_in_tolerance()

    def _are_req_poses_in_range(self):
        assert self._coll is not None
        assert self._coll.min is not None
        assert self._coll.max is not None

        return (
            self._coll.min <= self._req_pos1 <= self._coll.max
            and self._coll.min <= self._req_pos2 <= self._coll.max
            and self._req_pos1 < self._req_pos2
        )

    def _is_pos1_in_tolerance(self):
        cur_pos1 = self._coll.pos1
        req_pos1 = self._req_pos1

        if cur_pos1 is None or req_pos1 is None:
            return None

        return abs(cur_pos1 - req_pos1) <= self._tolerance

    def _is_pos2_in_tolerance(self):
        cur_pos2 = self._coll.pos2
        req_pos2 = self._req_pos2

        if cur_pos2 is None or req_pos2 is None:
            return None

        return abs(cur_pos2 - req_pos2) <= self._tolerance

    def _is_first_jaw_going_first(self):
        assert self._req_pos1 is not None
        assert self._req_pos2 is not None
        assert self._coll.pos1 is not None
        assert self._coll.pos2 is not None

        cur_pos1 = self._coll.pos1
        req_pos1 = self._req_pos1
        cur_pos2 = self._coll.pos2
        req_pos2 = self._req_pos2
        if req_pos2 < cur_pos1:
            return True
        elif req_pos1 > cur_pos2:
            return False
        return True

    def _get_fist_motor(self):
        if self._is_first_jaw_going_first():
            return self._motor1
        else:
            return self._motor2

    def _get_second_motor(self):
        if self._is_first_jaw_going_first():
            return self._motor2
        else:
            return self._motor1

    def _is_minimum_gap_respected(self):
        minimum_gap = (
            0.5  # TODO: get Collimator.MINIMUM_GAP_MM avoiding circular import
        )
        return abs(self._req_pos2 - self._req_pos1) > minimum_gap

    def _is_running(self):
        # TODO: check motors status as well
        if (
            self._motor1_sub is None
            or self._motor2_sub is None
            or self._motor_thread2 is None
            or self._motor_thread1 is None
        ):
            return False

        return (
            self._motor1_sub.running
            or self._motor2_sub.running
            or self._motor_thread1.is_alive()
            or self._motor_thread2.is_alive()
        )

    @property
    def running(self):
        return self._is_running()

    @property
    def req_pos1(self):
        return self._req_pos1

    @property
    def req_pos2(self):
        return self._req_pos2
