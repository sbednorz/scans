import sys
import time

from .subscribers import DeviceSubscriber, DeviceSetter
from .callbacks import SubscriberCallback


class _MagnetCurrentSetterCallback(SubscriberCallback):
    def __init__(self, magnet_current_setter, magnet, req_current):
        super().__init__()
        self._magnet_current_setter = magnet_current_setter
        self._magnet = magnet
        self._req_current = req_current

        self._exc_info = None

    def _callback(self, cur_current):
        if self._magnet_current_setter.is_in_tolerance(cur_current):
            self._magnet_current_setter._sub.stop()

        self._magnet.acquire_without_current()
        # TODO: WE DO THIS TO IMMEDIATELY UPDATE THE CURRENT IN THE MODEL, BUT SHOULD BE DONE PROPERRLY
        self._magnet._current = cur_current

    def callback(self, *args, **kwargs):
        try:
            cur_current = args[1]
            self._callback(cur_current)
        except Exception:
            self._exc_info = sys.exc_info()


class MagnetCurrentSetter:
    CURRENT_PROPERTY_NAME = "current"

    def __init__(self, magnet, req_current, tolerance):
        # TODO assert isinstance(magnet, Magnet) avoiding circular imports
        assert isinstance(req_current, (float, int))
        assert isinstance(tolerance, (float, int))
        assert magnet.correct

        self._magnet = magnet
        self._req_cur = req_current
        self._tolerance = tolerance

        if not self._is_req_current_in_range():
            raise ValueError("Request current is not in range")

        self._callback = self._build_callback(self._magnet, self._req_cur)
        self._sub = self._build_sub(self._magnet, self._callback)
        self._setter = self._build_setter(self._magnet)

    def _is_req_current_in_range(self):
        assert self._req_cur is not None
        assert self._magnet is not None
        assert self._magnet.min is not None
        assert self._magnet.max is not None

        return self._magnet.min <= self._req_cur <= self._magnet.max

    def _build_callback(self, magnet, req_current):
        return _MagnetCurrentSetterCallback(self, magnet, req_current)

    def _build_sub(self, magnet, callback):
        return DeviceSubscriber(
            magnet,
            MagnetCurrentSetter.CURRENT_PROPERTY_NAME,
            [callback],
        )

    def _build_setter(self, magnet):
        return DeviceSetter(magnet, MagnetCurrentSetter.CURRENT_PROPERTY_NAME)

    def _is_running(self):
        if self._sub is None:
            return False

        if not self._sub.running:
            return False

        return True

    def is_in_tolerance(self, cur_current):
        return abs(cur_current - self._req_cur) < self._tolerance

    def launch(self):
        self._setter.set(self._req_cur)
        self._magnet.acquire_without_current()
        self._sub.start()

    def wait(self):
        while self.running:
            time.sleep(0.01)

    def stop(self):
        self._sub.stop()

    @property
    def running(self):
        return self._is_running()
