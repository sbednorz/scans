from .steering_settings import SteeringSettings


class CollimatorSettings(SteeringSettings):
    """Represents collimator scan settings. Based on CESAR scans GUI."""

    DEFAULT_START = -5.0
    DEFAULT_END = 5.0
    DEFAULT_STEPS = 3
    DEFAULT_SLIT = 2
    DEFAULT_TOLERANCE = 0.3

    def __init__(self):
        self.clear()

    def __str__(self):
        return (
            f"start: {self.start}\n"
            f"end: {self.end}\n"
            f"step: {self.step}\n"
            f"slit: {self.slit}\n"
            f"tolerance: {self.tolerance}\n"
        )

    def __repr__(self):
        return str(self)

    def _is_set(self):
        return (
            self._start is not None
            and self._end is not None
            and self._step is not None
            and self._slit is not None
            and self._tolerance is not None
        )

    def _is_valid(self):
        start = self.start
        end = self.end
        step = self.step
        slit = self.slit
        tolerance = self.tolerance
        return (
            tolerance > 0
            and start < end
            and step <= (end - start)
            and step > 0.0
            and slit > 0.0
        ) or (
            tolerance > 0
            and start > end
            and step <= (start - end)
            and step < 0.0
            and slit > 0.0
        )

    def _is_correct(self):
        return self._is_set() and self._is_valid()

    def clear(self):
        self._start = CollimatorSettings.DEFAULT_START
        self._end = CollimatorSettings.DEFAULT_END
        self._step = CollimatorSettings.DEFAULT_STEPS
        self._slit = CollimatorSettings.DEFAULT_SLIT
        self._tolerance = CollimatorSettings.DEFAULT_TOLERANCE

    @property
    def correct(self):
        return self._is_correct()

    @property
    def slit(self):
        return self._slit

    @slit.setter
    def slit(self, value):
        assert isinstance(value, (float, int))
        # TODO: assert value >= CollimatorPosSetter.MINIMUM_GAP_MM
        self._slit = value
