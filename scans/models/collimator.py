from enum import Enum

from .steering import Steering
from .motor import Motor
from .collimator_pos_setter import CollimatorPosSetter
from .device import Orientation
from .subscribers import DeviceGetter


class Collimator(Steering):
    """Represents a collimator device with two motors."""

    MINIMUM_GAP_MM = 0.5
    UNITS_NAME = "[mm]"

    def __init__(self, name):
        """Initialize a collimator device.

        Args:
            name (str): name of the collimator device, e.g. "XCSV.021.649".
        """
        super().__init__(name)
        self.clear()
        self.acquire()

    def __str__(self):
        return (
            f"pos1: {self.pos1}\n"
            f"pos2: {self.pos2}\n"
            f"min: {self.min}\n"
            f"max: {self.max}\n"
            f"ref1: {self.pos2_ref}\n"
            f"ref2: {self.pos1_ref}\n"
            f"slit: {self.slit}\n"
        )

    def __repr__(self):
        return str(self)

    def _is_double(self):
        """TODO: Always returns False.

        Should return True if the collimator is double. Not supported yet due
        to the lack of CESAR API.
        """
        return False

    def _motor_name(self, motor_no):
        """Returns the expected name of the motor.

        It assumes the collimator/motors naming convention is respected.
        """
        orientation = self.orientation.value
        name = self.name.replace(".", "_")
        return f"{name}_{motor_no}{orientation}"

    def _is_set(self):
        """Verifies if the collimator model is initialized."""
        return (
            self._pos1_ref is not None
            and self._pos2_ref is not None
            and isinstance(self._motor1, Motor)
            and isinstance(self._motor2, Motor)
        )

    def _is_correct(self):
        """Verifies if the collimator model is initialized and correct."""
        return self._is_set()

    def _acquire_pos1_ref(self):
        return float(DeviceGetter(self, "position1Reference").get())

    def _acquire_pos2_ref(self):
        return float(DeviceGetter(self, "position2Reference").get())

    def clear(self):
        """Deinitializes the collimator model."""
        self._pos1_ref = None
        self._pos2_ref = None
        self._motor1 = Motor(self._motor_name(1))
        self._motor2 = Motor(self._motor_name(2))

    def acquire(self):
        """Acquires collimator and its motors data."""
        self._pos1_ref = self._acquire_pos1_ref()
        self._pos2_ref = self._acquire_pos2_ref()
        self._motor1.acquire()
        self._motor2.acquire()

    def set_poses(self, req_pos1, req_pos2, tolerance):
        """Set the collimator motors to the requested positions."""
        assert isinstance(req_pos1, float)
        assert isinstance(req_pos2, float)

        setter = CollimatorPosSetter(self, req_pos1, req_pos2, tolerance)
        setter.launch()
        return setter

    @property
    def operational(self):
        """Verifies if the collimator is operational."""
        return self._motor1.operational and self._motor2.operational

    @property
    def correct(self):
        return self._is_correct()

    @property
    def motor1(self):
        return self._motor1

    @property
    def motor2(self):
        return self._motor2

    @property
    def orientation(self):
        """Returns the orientation of the collimator basing on its name.

        Assumes the collimator/motors naming convention is respected.
        """
        if self._is_double():
            raise NotImplementedError("double collimators are not supported yet")
        else:
            return Orientation(self.name.split(".")[0][-1])

    @property
    def pos1_ref(self):
        return self._pos1_ref

    @property
    def pos2_ref(self):
        return self._pos2_ref

    @property
    def pos1(self):
        return self._motor1.pos

    @property
    def pos2(self):
        return self._motor2.pos

    @property
    def min(self):
        return self._motor1.min

    @property
    def max(self):
        return self._motor2.max

    @property
    def slit(self):
        if self.pos2 is None or self.pos1 is None:
            return None
        return self.pos2 - self.pos1

    @property
    def min_slit(self):
        return Collimator.MINIMUM_GAP_MM

    @property
    def max_slit(self):
        if self.max is None or self.min is None:
            return None
        return self.max - self.min

    @property
    def center(self):
        if self.pos1 is None or self.slit is None:
            return None
        return self.pos1 + self.slit / 2

    @property
    def jaw1_name(self):
        if self.orientation == Orientation.VERTICAL:
            return "Down"
        elif self.orientation == Orientation.HORIZONTAL:
            return "Left"
        else:
            raise ValueError("Bad orientation")

    @property
    def jaw2_name(self):
        if self.orientation == Orientation.VERTICAL:
            return "Up"
        elif self.orientation == Orientation.HORIZONTAL:
            return "Right"
        else:
            raise ValueError("Bad orientation")

    @property
    def units_name(self):
        return Collimator.UNITS_NAME
