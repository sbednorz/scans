from .steering import Steering
from .subscribers import DeviceGetter


class Tax(Steering):
    """Represents a tax device with two motors.

    TODO: Not implemented fully yet and not used .
    """

    def __init__(self, name):
        super().__init__(name)
        self._min = None
        self._max = None
        self._pos = None
        self._ref = None

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def pos(self):
        return self._pos

    @property
    def ref(self):
        return self._ref

    @min.setter
    def min(self, value):
        self._min = value

    @max.setter
    def max(self, value):
        self._max = value

    @pos.setter
    def pos(self, value):
        self._pos = value

    @ref.setter
    def ref(self, value):
        self._ref = value


# TODO: Move to the Tax model
class TaxController2:
    def __init__(self, tax):
        self._model = tax

    def acquire(self):
        self._model.min = -666
        self._model.max = 666
        self._model.pos = float(DeviceGetter(self._model, "position").get())
        self._model.ref = float(DeviceGetter(self._model, "positionReference").get())

    @property
    def model(self):
        return self._model
