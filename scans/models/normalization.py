from .detector import Detector


class Normalization:
    def __init__(self, name, detector, threshold):
        assert isinstance(name, str)
        assert isinstance(detector, Detector) or detector is None
        assert isinstance(threshold, (float, int))

        self._name = name
        self._detector = detector
        self._threshhold = threshold

    def normalize(self, value_to_normalize):
        if self.detector is None:
            return value_to_normalize

        normalization_value = self._detector.count

        if not self.has_beam(normalization_value):
            # TODO: this case should be handled correctly by the GUI
            raise RuntimeError("No beam.")

        return value_to_normalize / normalization_value

    def has_beam(self, count):
        return count >= self.threshold

    @property
    def no_normalization(self):
        return self.detector is None

    @property
    def name(self):
        return self._name

    @property
    def detector(self):
        return self._detector

    @property
    def threshold(self):
        return self._threshhold


class Normalizations:
    @staticmethod
    def build_tbiu():
        return Normalization("TBIU", Detector("BSI.230.705"), 1e8)

    @staticmethod
    def build_tbid():
        return Normalization("TBID", Detector("BSI.230.950"), 1e4)

    @staticmethod
    def build_no_normalization():
        return Normalization("No normalization", None, 0)

    @staticmethod
    def build_all():
        return [
            Normalizations.build_tbiu(),
            Normalizations.build_tbid(),
            Normalizations.build_no_normalization(),
        ]
