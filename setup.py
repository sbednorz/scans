"""
setup.py for scans.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path
from setuptools import setup, find_packages


HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt', encoding='utf-8') as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    'core': [
        'ipython',
        'ipywidgets',
        'numpy',
        'numpy',
        'numpy',
        'pandas',
        'plotly',
        'pyjapc',
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        'jupyterlab',
        'jupyterlab-widgets',
    ],
    'doc': [
        'sphinx',
        'acc-py-sphinx',
    ],
}


setup(
    name='scans',
    version="0.0.1.dev0",

    author='Szymon Marek Bednorz',
    author_email='szymon.bednorz@cern.ch',
    description='CESAR scans PoC with Python.',
    long_description='Proof of concept collection of Jupyter notebook widgets to perform and visualize CESAR scans.',
    long_description_content_type='text/markdown',
    url='',

    packages=find_packages(),
    python_requires='~=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },
)
