# Scans

A proof of concept Python library with a collection of Jupyter notebook widgets to perform and visualize CESAR scans.

## Getting started
While the project hasn't been officially distributed as an acc-py package yet, you can still use its developmental version:

1. [Initialize acc-py](https://wikis.cern.ch/display/ACCPY/Getting+started+with+Acc-Py):
    ```bash
    $ source /acc/local/share/python/acc-py/base/pro/setup.sh
    ```
2. Clone this repository.
3. Create a new Python virtual environment:
    ```bash
    $ acc-py venv /path/to/my/venv
    ```
4. Start using the created virtual environment:
    ```bash
    $ source /path/to/my/venv/bin/activate
    ```
5. Install the necessary dependencies:
    ```bash
    $ python -m pip install -r requirements.txt
    ```
6. Start JupyterLab (or other Jupyter-based editor) and explore demo notebooks in `notebooks` directory:
    ```bash
    $ python -m jupyter lab notebooks
    ```

## Usage
This project is intended to be used in Jupyter environemnt with its interactive widgets support. That said, core functionalities can be accessed directly through the Python module without any graphical interfaces.

### With JupyterLab
A sample notebook is available at [notebooks/simple_scans.ipynb](notebooks/simple_scans.ipynb) showing a basic scan process:
```bash
$ python -m jupyter lab notebooks/scans_demo.ipynb
```
It will start the JupyterLab server and open the editor page in your browser.

### With Voilà
Voilà allows you to convert a Jupyter Notebook into an interactive web dashboard that allows you to share your work with others and hide the Python code. [Read more about Voilà here](https://voila.readthedocs.io/en/stable/using.html).


To run a Voilà demo:
```bash
$ voila notebooks/navigator.ipynb
```
It will start the Voilà server and open the generated page in your browser.

### With CERN SWAN (TBD)
It is possible to install all required dependencies in CERN SWAN environment. However, at the moment of writing this document, SWAN cannot be used with Technical Network, which is required to access CESAR devices and performing the scans.

## Repository structure
This repository is organized as follows:

- `scans`: the main Python module with the CESAR scans logic,
- `notebooks`:  Jupyter notebooks illustrating the library's application and various functionalities,
    -   `simple_scans.ipynb`: plug-and-play notebook dedicated to CESAR scans.
    -   `demo.ipynb`: showcase of ipywidgets and notebook capabilities,
    -   `navigator.ipynb`, `child.ipynb`: demonstration of navigating through notebooks and [Voilà extension](https://voila.readthedocs.io/en/stable/#).

### Python module structure
The `scans` module is organized as follows:

- `cesar`: due to the lack of easy-accessible CESAR API, list of available devices for scans are stored in this module.
- `models`: classes representing CESAR devices (e.g. `Detector`, `Collimator`, `Magnet`), interactions with them (e.g. `CollimatorPosSetter`, `MagnetCurrentSetter`), and device access using JAPC.
- `views`: everything related to Jupyter user interface.
- `handlers`: high-level handlers responsible for scans procedure (e.g. `ScanCollimator`, `ScanMagnet`).
- `models/experimental`: experimental features and ideas which are not yet ready.

## Notes